# Women Engineers Program - Module 3 Assignments

This repository contains my work for the Module 3 assignments on Gen AI as part of the Women Engineers Program.

## Assignment I

The assignments involve various tasks ranging from solving computational problems using Gen AI, learning and playing the game of Yahtzee with Gen AI, creating a testing strategy for a Yahtzee scorer, and writing a Python function for text generation using Markov chains.

### Assignment 01

In this assignment, I picked a non-trivial problem statement from my computational thinking course and asked Gen AI to solve it. I then iterated upon its solution to apply our principles of clean code.

### Assignment 02

This assignment involved using a Gen AI tool to learn what the game of Yahtzee is, engineering the Gen AI tool to play Yahtzee with me, and asking the Gen AI tool how to approach writing code for a Yahtzee scorer myself.

### Assignment 03

In this assignment, I created a testing strategy for the Yahtzee scorer code that was generated and documented my journey.

### Assignment 04

This assignment involved writing a Python function `generate(filename: str, start_words: list[str], chain_length: int, num_generated: int) -> str` which takes a filename, a chain length, a list of start words which has to be exactly as long as the chain_length, and an integer num_generated and returns a sentence num_generated words long which sounds similar to the text contained in filename.

## Assignment II

This assignment involves writing an essay/report in 500 - 1000 words on "developing strategies for the bidding card game 'Diamonds' with GenAI".

## Assignment III

This assignment involves using Gen AI tool to develop a UI for the diamonds game using the pygame python module. A report also needs to be prepared in 500 - 1000 words on the observations thus, made.
