from gameplay import Gameplay

def main():
    name = input("Enter your name: ")
    gameplay = Gameplay(name)
    gameplay.run_game()

if __name__ == "__main__":
    main()
